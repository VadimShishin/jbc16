package DZ_3_1.Task06;

import java.util.Arrays;

public class AmazingString {
    char[] chars;

    public AmazingString(char[] chars) {
        this.chars = chars;
    }

    public AmazingString(String s) {
        this.chars = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            this.chars[i] = s.charAt(i);
        }
    }

    public char[] getChars() {
        return chars;
    }

    public void setChars(char[] chars) {
        this.chars = chars;
    }

    public String charsToString(char[] arrayChars) {
        return new String(arrayChars);
    }

    // Вернуть i-ый символ строки
    public char charFromString(String s, int n) {
        char[] chars = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            chars[i] = s.charAt(i);
        }
        return chars[n];
    }

    //  Вернуть длину строки
    public int lengthString(String s) {
        int size = 0;
        char[] chars = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            chars[i] = s.charAt(i);
            size++;
        }
        return size;
    }

    // Вывести строку на экран
    public void displayString(char[] arrayChars) {
        String str = new String(arrayChars);
        System.out.println(str);
    }

    // Проверить, есть ли переданная подстрока в AmazingString (на вход подается массив char). Вернуть true, если найдена и false иначе
    public boolean checkChars(char[] array) {
        return Arrays.equals(this.chars, array);
    }

    // Проверить, есть ли переданная подстрока в AmazingString (на вход подается String). Вернуть true, если найдена и false иначе
    public boolean checkString(String s) {
        char[] array = new char[s.length()];
        for (int i = 0; i < s.length(); i++) {
            array[i] = s.charAt(i);
        }

        return Arrays.equals(this.chars, array);
    }

    // Удалить из строки AmazingString ведущие пробельные символы, если они есть
    public static void removeSpace(String s) {
        char[] array = new char[s.length()];

        int size = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                size++;
            }
            array[i] = s.charAt(i);
        }

        char[] arrayWithoutSpaces = new char[s.length() - size];
        int temp = 0;

        for(int i = 0; i < arrayWithoutSpaces.length + size; i++){
            if(array[i] != ' ') {
                arrayWithoutSpaces[temp] = array[i];
                temp++;
            }
        }

        String string = new String(arrayWithoutSpaces);
        System.out.println(string);
    }

    // Развернуть строку (первый символ должен стать последним, а последний первым и т.д.)
    public static void reverseString(String s){
        char[] array = new char[s.length()];
        char[] arrayReverse = new char[s.length()];

        for (int i = 0; i < s.length(); i++) {
            array[i] = s.charAt(i);
        }

        int number = s.length() - 1;
        for (int i = 0; i < array.length; i++) {
            arrayReverse[i] = array[number];
            number--;
        }

        String result = new String(arrayReverse);
        System.out.println(result);
    }

    public static void main(String[] args) {
        AmazingString s1 = new AmazingString("Робокоп");
        AmazingString s2 = new AmazingString("Робокоп");
        AmazingString s3 = new AmazingString("Робокоп, Ваня и Ципа");
        System.out.println(s1.charFromString(s1.charsToString(s1.getChars()), 0));
        System.out.println(s1.lengthString(s1.charsToString(s1.getChars())));
        s1.displayString(s1.getChars());
        System.out.println(s1.checkChars(s2.getChars()));
        System.out.println(s1.checkString("Рыбокоп"));
        removeSpace(s3.charsToString(s3.getChars()));
        reverseString(s1.charsToString(s1.getChars()));
    }
}