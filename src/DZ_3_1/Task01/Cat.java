package DZ_3_1.Task01;

/*
Необходимо реализовать класс Cat.
У класса должны быть реализованы следующие приватные методы:
- sleep() — выводит на экран “Sleep”
- meow() — выводит на экран “Meow”
- eat() — выводит на экран “Eat”
И публичный метод:
status() — вызывает один из приватных методов случайным образом.
 */

class Cat {
    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }

    public void status() {
        int n = (int) (Math.random() * 3);

        if (n == 0) sleep();
        if (n == 1) meow();
        if (n == 2) eat();
    }

    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.status();
    }
}
