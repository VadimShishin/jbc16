package DZ_3_1.Task02;

/*
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
-	String name — имя студента
-	String surname — фамилия студента
-	int[] grades — последние 10 оценок студента. Их может быть меньше, но не может быть больше 10.
И следующие публичные методы:
-	геттер/сеттер для name
-	геттер/сеттер для surname
-	геттер/сеттер для grades
-	метод, добавляющий новую оценку в grades. Самая первая оценка должна быть удалена, новая должна сохраниться в конце массива (т.е. массив должен сдвинуться на 1 влево).
-	метод, возвращающий средний балл студента (рассчитывается как среднее арифметическое от всех оценок в массиве grades)
 */

public class Student {
    private String name;
    private String surname;
    private int[] grades = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public void addGrade(int grade) {
        for (int i = 0; i < 1; i++) {
            if (grades.length - 1 >= 0) System.arraycopy(grades, 1, grades, 0, grades.length - 1);
            grades[grades.length - 1] = grade;
        }
//        System.out.println(Arrays.toString(grades));
    }

    public int gpa() {
        int result = 0;
        for (int grade : grades) {
            result +=  grade;
        }
        return result / grades.length;
    }

    public static void main(String[] args) {
        //test
        Student student = new Student();
        student.addGrade(5);
        System.out.println(student.gpa());
    }
}
