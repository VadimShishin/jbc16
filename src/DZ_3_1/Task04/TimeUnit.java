package DZ_3_1.Task04;

/*
    Необходимо реализовать класс TimeUnit с функционалом, описанным ниже (необходимые поля продумать самостоятельно).
    Обязательно должны быть реализованы валидации на входные параметры.
Конструкторы:
●	Возможность создать TimeUnit, задав часы, минуты и секунды.
●	Возможность создать TimeUnit, задав часы и минуты. Секунды тогда должны проставиться нулевыми.
●	Возможность создать TimeUnit, задав часы. Минуты и секунды тогда должны проставиться нулевыми.
Публичные методы:
●	Вывести на экран установленное в классе время в формате hh:mm:ss
●	Вывести на экран установленное в классе время в 12-часовом формате (используя hh:mm:ss am/pm)
●	Метод, который прибавляет переданное время к установленному в TimeUnit (на вход передаются только часы, минуты и секунды).

 */

public class TimeUnit {
    private int hour;
    private int minute;
    private int second;

    public TimeUnit(int hour, int minute, int second) {
        if (hour >= 0 && hour <= 23) {
            this.hour = hour;
        } else System.out.println("Установлено некорректное количество часов");

        if (minute >= 0 && minute <= 59)
            this.minute = minute;
        else System.out.println("Установлено некорректное количество минут");

        if (second >= 0 && second <= 59)
            this.second = second;
        else System.out.println("Установлено некорректное количество секунд");
    }

    public TimeUnit(int hour, int minute) {
        if (hour >= 0 && hour <= 23) {
            this.hour = hour;
        } else System.out.println("Установлено некорректное количество часов");

        if (minute >= 0 && minute <= 59)
            this.minute = minute;
        else System.out.println("Установлено некорректное количество минут");

        second = 0;
    }

    public TimeUnit(int hour) {
        if (hour >= 0 && hour <= 23) {
            this.hour = hour;
        } else System.out.println("Установлено некорректное количество часов");

        minute = 0;
        second = 0;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    public void setHour(int hour) {
        if (hour >= 0 && hour <= 23)
            this.hour = hour;
        else System.out.println("Установлено некорректное количество часов");
    }

    public void setMinute(int minute) {
        if (minute >= 0 && minute <= 59)
            this.minute = minute;
        else System.out.println("Установлено некорректное количество минут");
    }

    public void setSecond(int second) {
        if (second >= 0 && second <= 59)
            this.second = second;
        else System.out.println("Установлено некорректное количество секунд");
    }

    //Вывести на экран установленное в классе время в формате hh:mm:ss
    public void displayTime() {
        if (getHour() < 10) System.out.print("0" + getHour());
        else System.out.print(getHour());

        if (getMinute() < 10) System.out.print(":" + "0" + getMinute());
        else System.out.print(":" + getMinute());

        if (getSecond() < 10) System.out.println(":" + "0" + getSecond());
        else System.out.println(":" + getSecond());
    }

    //Вывести на экран установленное в классе время в 12-часовом формате
    public void formatDisplayTime() {
        int hourTemp = getHour();
        if (hourTemp == 0) hourTemp = 12;
        if (getHour() > 12) {
            hourTemp = getHour() - 12;
            if (hourTemp < 10) System.out.print("0" + hourTemp);
            else System.out.print(hourTemp);

            if (getMinute() < 10) System.out.print(":" + "0" + getMinute());
            else System.out.print(":" + getMinute());

            if (getSecond() < 10) System.out.println(":" + "0" + getSecond());
            else System.out.println(":" + getSecond());
        } else {
            if (hourTemp < 10) System.out.print("0" + hourTemp);
            else System.out.print(hourTemp);

            if (getMinute() < 10) System.out.print(":" + "0" + getMinute());
            else System.out.print(":" + getMinute());

            if (getSecond() < 10) System.out.println(":" + "0" + getSecond());
            else System.out.println(":" + getSecond());
        }
    }

    //прибавляет переданное время к установленному в TimeUnit
    public void addTime(int hour, int minute, int second) {
        if (hour >= 0 && hour <= 23) {
            if (hour + this.hour <= 23) {
                this.hour += hour;
            } else this.hour = (this.hour + hour) - 24;
        } else System.out.println("Установлено некорректное количество часов");

        if (minute >= 0 && minute <= 59) {
            if (minute + this.minute <= 59) {
                this.minute += minute;
            } else this.minute = (this.minute + minute) - 60;
        } else System.out.println("Установлено некорректное количество минут");

        if (second >= 0 && second <= 59) {
            if (second + this.second <= 59) {
                this.second += second;
            } else this.second = (this.second + second) - 60;
        } else System.out.println("Установлено некорректное количество секунд");
    }

    public static void main(String[] args) {

        //tests
        TimeUnit timeUnit = new TimeUnit(1, 5, 10);
        timeUnit.setHour(13);
        timeUnit.setMinute(14);
        timeUnit.setSecond(15);
        timeUnit.addTime(11, 4, 30);
        timeUnit.formatDisplayTime();
        timeUnit.displayTime();
    }
}