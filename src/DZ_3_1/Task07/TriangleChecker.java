package DZ_3_1.Task07;

public class TriangleChecker {
    double a, b, c;

    public static boolean triangle(double a, double b, double c){
        return a + b > c && a + c > b && b + c > a;
    }

    public static void main(String[] args) {
        System.out.println(triangle(4.3, 5.2, 3.1));
        System.out.println(triangle(2, 1, 3.1));
    }
}
