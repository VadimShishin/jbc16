package DZ_3_1.Task08;
/*
Реализовать класс “банкомат” Atm.
Класс должен:
- Содержать конструктор, позволяющий задать курс валют перевода долларов в рубли и курс валют перевода рублей в доллары.
- Содержать два публичных метода, которые позволяют переводить переданную сумму рублей в доллары и долларов в рубли.
- Хранить приватную переменную счетчик — количество созданных инстансов класса Atm и публичный метод, возвращающий этот
счетчик (подсказка: реализуется через static).
 */
public class Atm {
    private int dollars;
    private int rubles;

    private static int counter;

    public Atm(int i, String currency) {
        if (currency.equalsIgnoreCase("rubles")) {
            this.rubles = i;
            counter++;
        }
        if (currency.equalsIgnoreCase("dollars")) {
            this.dollars = i;
            counter++;
        }
    }

    public int getDollars() {
        return dollars;
    }

    public int getRubles() {
        return rubles;
    }


    // МЕТОДЫ
    public int convertDollarsToRubles(){
        return dollars * 70;
    }

    public int convertRublesToDollars(){
        return rubles / 70;
    }

    public static void showCount(){
        System.out.println(counter);
    }

    public static void main(String[] args) {
        Atm atm1 = new Atm(100, "dollars");
        Atm atm2 = new Atm(5000, "rubles");
        System.out.println("Сумма " + atm1.getDollars() + " $"  + ", конвертировано в " + atm1.convertDollarsToRubles() + " ?");
        System.out.println("Сумма " + atm2.getRubles() + " ?"  + ", конвертировано в " + atm2.convertRublesToDollars() + " $");
        showCount();
    }
}