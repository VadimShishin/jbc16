package DZ_3_1.Task05;
/*
���������� ����������� ����� DayOfWeek ��� �������� ����������� ������ ��� ������ (byte) � �������� ��� ������ (String).
����� � ��������� ������ � ������ main ������� ������ �������� DayOfWeek ����� 7.
��������� ��� ���������������� ����������(�� 1 Monday �� 7 Sunday) � ������� �������� ������� �������� DayOfWeek �� �����.
*/

public class DayOfWeek {
    private byte number;
    private String day;


    public DayOfWeek(byte number, String day) {
        this.number = number;
        this.day = day;
    }

    public byte getNumber() {
        return number;
    }

    public void setNumber(byte number) {
        this.number = number;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}