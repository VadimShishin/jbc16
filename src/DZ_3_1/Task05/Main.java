package DZ_3_1.Task05;
/*
���������� ����������� ����� DayOfWeek ��� �������� ����������� ������ ��� ������ (byte) � �������� ��� ������ (String).
����� � ��������� ������ � ������ main ������� ������ �������� DayOfWeek ����� 7.
��������� ��� ���������������� ����������(�� 1 Monday �� 7 Sunday) � ������� �������� ������� �������� DayOfWeek �� �����.
*/

public class Main {
    public static void main(String[] args) {
        String[] days = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        DayOfWeek[] dayOfWeek = new DayOfWeek[7];

        for (int i = 0; i < dayOfWeek.length; i++) {
            dayOfWeek[i] = new DayOfWeek((byte) (i + 1), days[i]);
        }


        for (DayOfWeek n : dayOfWeek) {
            System.out.println(n.getNumber() + " " + n.getDay());
        }
    }
}