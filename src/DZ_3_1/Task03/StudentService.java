package DZ_3_1.Task03;

import DZ_3_1.Task02.Student;

/*
Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
- bestStudent() — принимает массив студентов (класс Student из предыдущего задания), возвращает лучшего студента
(т.е. который имеет самый высокий средний балл). Если таких несколько — вывести любого.
- sortBySurname() — принимает массив студентов (класс Student из предыдущего задания) и сортирует его по фамилии.

 */

import java.util.Arrays;
import java.util.Comparator;

public class StudentService {
    public static void main(String[] args) {
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();

        student1.setSurname("Сидоров");
        student2.setSurname("Иванов");
        student3.setSurname("Петров");

        student1.addGrade(9);
        student2.addGrade(1);
        student3.addGrade(2);

        Student[] arrStudents = {student1, student2, student3};

        System.out.println(bestStudent(arrStudents).getSurname());

        sortBySurname(arrStudents);
//        for (Student student : arrStudents){
//            System.out.println(student.getSurname());
//        }
    }

    public static Student bestStudent(Student[] students) {
        int index = 0;
        for (int i = 0; i < students.length; i++) {
            for (int j = 0; j < students.length; j++) {
                if (students[i].gpa() > students[j].gpa()){
                    index = i;
                }
            }
        }
        return students[index];
    }

    public static void sortBySurname(Student[] students) {
        Arrays.sort(students, Comparator.comparing(Student::getSurname));
    }
}