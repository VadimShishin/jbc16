package DZ_2_1;

import java.util.Scanner;
/**
���������� ����� � ������� �����, ����������� ������� � M (�.�. ����� �����, ��� �������� |ai - M| �����������).
 ���� �� ���������, �� ������� ������������ �����.
**/

public class Task08 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] array = new int[input.nextInt()];
        for (int i = 0; i < array.length; i++) {
            array[i] = input.nextInt();
        }

        System.out.println(task(input.nextInt(), array));
    }

    public static int task(int r, int[] n) {
        int min = n[0];
        int subMin = Math.abs(n[0] - r);
        for (int i = 1; i < n.length; i++) {
            int temp = Math.abs(n[i] - r);
            if (temp <= subMin) {
                subMin = temp;
                min = n[i];
            }
        }
        return min;
    }
}