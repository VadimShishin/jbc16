package DZ_2_1;

import java.util.Arrays;
import java.util.Scanner;
/**
���������� ������� ������, ���������� �� ��������� ����������� � ������� ������� ��������, ����������� �������� �� ����������� � ������� �� �� �����.
*/

public class Task07 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int size = input.nextInt();
        int[] array = new int[size];

        for (int i = 0; i < array.length; i++) {
            array[i] = input.nextInt();
        }

        for (int i : elementsInASquare(array)){
            System.out.print(i + " ");
        }
    }

    public static int[] elementsInASquare(int[] arr){
        int[] array2 = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            array2[i] = (int) Math.pow(arr[i], 2);
        }
        Arrays.sort(array2);
        return array2;
    }
}