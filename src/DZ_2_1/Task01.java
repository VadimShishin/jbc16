package DZ_2_1;

import java.util.Scanner;
/**
На вход подается число N — длина массива. Затем передается массив вещественных чисел (ai) из N элементов.
Необходимо реализовать метод, который принимает на вход полученный массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран
**/
public class Task01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int size = input.nextInt();

        double array[] = new double[size];

        for (int i = 0; i < size; i++) {
            array[i] = input.nextDouble();
        }

        System.out.println(sum(array));

    }
    public static double sum(double[] array){
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        sum = sum / array.length;
        return sum;
    }
}