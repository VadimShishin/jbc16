package DZ_2_1;

import java.util.Scanner;

//���������� ���������� �������� �������� ������� �� M ��������� ������.

public class Task05 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int size = input.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = input.nextInt();
        }

        int shiftRight = input.nextInt();

        int[] b = new int[array.length];
        if (shiftRight > 0) {
            shiftRight = shiftRight + 1;
            System.arraycopy(array, shiftRight, b, 0, array.length - shiftRight);
            System.arraycopy(array, 0, b, array.length - shiftRight, shiftRight);
            for (int i : b) {
                System.out.print(i + " ");
            }
        } else {
            for (int i : array) {
                System.out.print(i + " ");
            }
        }
    }
}