package DZ_2_1;

import java.util.Arrays;
import java.util.Scanner;

public class TaskDop02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int[] array = new int[input.nextInt()];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) Math.pow(input.nextInt(), 2);
        }

        insertionSort(array);
        System.out.println(Arrays.toString(array));
    }
    public static void insertionSort(int[] sortArr) {
        int j;
        for (int i = 1; i < sortArr.length; i++) {
            int swap = sortArr[i];
            for (j = i; j > 0 && swap < sortArr[j - 1]; j--) {
                sortArr[j] = sortArr[j - 1];
            }
            sortArr[j] = swap;
        }
    }
}