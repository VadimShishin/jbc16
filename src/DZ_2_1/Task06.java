package DZ_2_1;

import java.util.Scanner;

//��������� ���� � ������ �����

public class Task06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();

        System.out.println(txtToMorse(s));
    }

    public static String txtToMorse(String txt) {
        char[] alphabetChar = {'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�'};
        String[] morse = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

        String line = "";
        for (int i = 0; i < txt.length(); i++) {
            for (int j = 0; j < alphabetChar.length; j++) {
                char s = alphabetChar[j];
                if (txt.charAt(i) == s) {
                    line = line + morse[j] + " ";
                }
            }
        }
        return line;
    }
}