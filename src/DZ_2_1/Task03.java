package DZ_2_1;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 1. �� ���� �������� ����� N � ����� �������.
 * 2. ����� ���������� ������ ����� ����� (ai) �� N ���������, ��������������� �� �����������.
 * 3. ����� ����� �������� ����� X � �������, ������� ����� �������� � ������, ����� ���������� � ������� �����������.
 * 4. ���������� ������� �� ����� ������ �������� �������, ���� ����� �������� X.
 * ���� � ������� ��� ���� ����� ������ X, �� X ����� ��������� ����� ��� �������������.
 **/

public class Task03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int size = input.nextInt(); //�. 1 - �������� ����� N � ����� �������
        int[] array1 = new int[size]; // �������� ������

        for (int i = 0; i < array1.length; i++) { //�. 2 - ���������� ������ ����� ����� (ai) �� N ���������
            array1[i] = input.nextInt();
        }

        Arrays.sort(array1); // �. 2 - ���������� �������

        int dopNum = input.nextInt(); //�. 3 - ������������� ��� �����

        int[] array2 = Arrays.copyOf(array1, array1.length + 1); //�. 3 �������� ����� ������
        array2[array2.length - 1] = dopNum; //�. 3 - ���������� ������ �������� � ����� ������
        Arrays.sort(array2); // �. 3 - ���������� �������

        System.out.println(linearSearch(array2, dopNum)); //�. 4
    }

    public static int linearSearch(int[] list, int key) { //����� ����������
        for (int i = list.length - 1; i >= 0; i--) {
            if (key == list[i])
                return i;
        }
        return -1;
    }
}