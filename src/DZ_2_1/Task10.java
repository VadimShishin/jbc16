package DZ_2_1;

import java.util.Scanner;

/**
 * Компьютер «загадывает» (с помощью генератора случайных чисел) целое число M в промежутке от 0 до 1000 включительно.
 * Затем предлагает пользователю угадать это число. Пользователь вводит число с клавиатуры.
 */

public class Task10 {
    public static void main(String[] args) {
        game();
    }

    public static void game() {
        Scanner input = new Scanner(System.in);
        int number = (int) (Math.random() * 1001);
        int result;
        System.out.println("Угадайте число от 0 до 1000 включительно.");

        do {
            System.out.print("Введите число: ");
            int enteredNumber = input.nextInt();
            result = enteredNumber;
            if (enteredNumber < number) {
                System.out.println("Это число меньше загаданного.");
            } else if (enteredNumber > number) {
                System.out.println("Это число больше загаданного.");
            }
        } while (result != number);
        System.out.println("Победа!");
    }
}