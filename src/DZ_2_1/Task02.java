package DZ_2_1;

import java.util.Arrays;
import java.util.Scanner;
/**
На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов.
 После этого аналогично передается второй массив (aj) длины M.
Необходимо вывести на экран true, если два массива одинаковы (то есть содержат одинаковое количество элементов
 и для каждого i == j элемент ai == aj). Иначе вывести false.
**/
public class Task02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int size1 = input.nextInt();
        int[] array1 = new int[size1];
        for (int a = 0; a < array1.length; a++) {
            array1[a] = input.nextInt();
        }

        int size2 = input.nextInt();
        int[] array2 = new int[size2];
        for (int b = 0; b < array1.length; b++) {
            array2[b] = input.nextInt();
        }

        if (array1.length == array2.length) {
            if (Arrays.equals(array1, array2)) {
                System.out.println(true);
            } else
                System.out.println(false);
        } else
            System.out.println(false);
    }
}