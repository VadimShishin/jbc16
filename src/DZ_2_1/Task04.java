package DZ_2_1;

import java.util.Scanner;
/**
�� ���� �������� ����� N � ����� �������. ����� ���������� ������ ����� ����� (ai) �� N ���������, ��������������� �� �����������.
���������� ������� �� ����� ��������� ������� ����������� ��������� ���������.
 ������ ������ ������ ��������� ���������� ��������� � ��� ������� ����� ������.
���������� ������� �� ����� ��������� ������� ����������� ��������� ��������� � ������� Java.
 ������ ������ ������ ��������� ���������� ��������� � ��� ������� ����� ������
**/
public class Task04 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int size = input.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = input.nextInt();
        }

        int[] arrayCopy = new int[array.length + 1];
        System.arraycopy(array, 0, arrayCopy, 0, array.length); // ����������� �������

        int count = 1;
        for (int i = 1; i < arrayCopy.length ; i++) {
            if (arrayCopy[i - 1] == arrayCopy[i]){
                count++;
            } else {
                System.out.println(count + " " + arrayCopy[i - 1]);
                count = 1;
            }
        }
    }
}