package DZ_3_2DOP;

import java.util.ArrayList;
import java.util.Scanner;

public class Library {
    protected static final Library LIBRARY = new Library();

    private Library() {
    }

    private final ArrayList<Book> books = new ArrayList<>();
    private final ArrayList<String> lendBooks = new ArrayList<>();
    private final ArrayList<Visitor> bookBorrower = new ArrayList<>();

    public void addBook(Book book) {
        boolean contains = false;
        for (Book b : books) {
            if (b.getName().equals(book.getName())) {
                contains = true;
                break;
            }
        }

        if (contains) {
            System.out.println("Книга \"" + book.getName() + "\" не добавлена, так как она уже есть в библиотеке.");
        } else {
            books.add(book);
            System.out.println("Книга \"" + book.getName() + "\" добавлена в библиотеку.");
        }
    }

    public void deleteBook(String nameBook) {
        boolean contains = false;
        for (Book b : books) {
            if (b.getName().equalsIgnoreCase(nameBook)) {
                contains = true;
                books.remove(b);
                break;
            }
        }

        if (contains)
            System.out.println("Книга \"" + nameBook + "\" удалена из библиотеки.");
        else
            System.out.println("Невозможно удалить книгу \"" + nameBook + "\", так как она не найдена в библиотеке.");
    }

    public String nameSearch(String nameBook) {
        String book = "Книга не найдена";
        for (Book b : books) {
            if (b.getName().equalsIgnoreCase(nameBook)) {
                book = b.getName();
                break;
            }
        }
        return book;
    }

    public void authorSearch(String authorBook) {
        ArrayList<String> list = new ArrayList<>();
        for (Book b : books) {
            if (b.getAuthor().equalsIgnoreCase(authorBook)) {
                list.add(b.getName());
            }
        }
        if (list.size() == 0) {
            System.out.println("Автор не найден");
        } else {
            for (String s : list) {
                System.out.println(s);
            }
        }
    }

    public void lendBook(String nameBook, Visitor visitor) {
        if (nameSearch(nameBook).equals("Книга не найдена")) {
            System.out.println("Книги \"" + nameBook + "\" нет в библиотеке.");
        } else {
            if (bookBorrower.contains(visitor)) {
                System.out.println("Можно взять только одну книгу.");
            } else if (lendBooks.contains(nameBook)) {
                System.out.println("Книга \"" + nameBook + "\" уже одолжена");
            } else {
                lendBooks.add(nameBook);
                bookBorrower.add(visitor);
                System.out.println(visitor.getName() + " одолжил книгу \"" + nameBook + "\"");
            }
        }
    }

    public void returnBook(String nameBook, Visitor visitor) {
        if (lendBooks.contains(nameBook)) {
            if (lendBooks.indexOf(nameBook) == bookBorrower.indexOf(visitor)) {
                lendBooks.remove(nameBook);
                bookBorrower.remove(visitor);
                System.out.println(visitor.getName() + " вернул книгу \"" + nameBook + "\"");
                gradeBook(nameBook);
            } else {
                System.out.println(visitor.getName() + " не брал книгу \"" + nameBook + "\"");
            }
        } else {
            System.out.println(visitor.getName() + " не брал книгу \"" + nameBook + "\"");
        }
    }

    public void gradeBook(String nameBook) {
        Scanner input = new Scanner(System.in);
        int grade;

        do {
            System.out.println("Оцените книгу \"" + nameBook + "\" по 10 бальной шкале");
            grade = input.nextInt();
        } while (grade < 0 || grade > 10);

        for (Book b : books) {
            if (b.getName().equalsIgnoreCase(nameBook)) {
                b.addGrade(grade);
                break;
            }
        }
    }

    public void showGradeBook(String nameBook) {
        for (Book b : books) {
            if (b.getName().equalsIgnoreCase(nameBook)) {
                System.out.println("Средняя оценка книги \"" + nameBook+ "\": " + b.getGrades());
                break;
            }
        }
    }

//        public void showBooks() {
//        System.out.println("В библиотеке содержатся следующие книги:");
//        for (Book b : books) {
//            System.out.println(b.getName() + ", " + b.getAuthor());
//        }
//    }
}