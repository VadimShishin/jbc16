package DZ_3_2DOP;

public class Visitor {
    private final String name;
    private final int id;
    private static int idInc = 1;

    public Visitor(String name) {
        this.name = name;
        this.id = idInc++;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
