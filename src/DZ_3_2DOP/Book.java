package DZ_3_2DOP;

import java.util.ArrayList;

public class Book {
    private final String name;
    private final String author;
    protected ArrayList<Integer> grades = new ArrayList<>();
    public Book(String name, String author) {
        this.name = name;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public void addGrade(int grade) {
        grades.add(grade);
    }

    public int getGrades() {
        int sum = 0;
        for (int grade : grades) {
            sum += grade;
        }
        return sum / grades.size();
    }
}
