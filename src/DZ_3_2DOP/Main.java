package DZ_3_2DOP;

import static DZ_3_2DOP.Library.LIBRARY;

public class Main {
    public static void main(String[] args) {
        //добавление книг
        System.out.println("ДОБАВЛЕНИЕ КНИГ:");
        LIBRARY.addBook(new Book("1984", "Джордж Оруэлл"));
        LIBRARY.addBook(new Book("Скотный двор", "Джордж Оруэлл"));
        LIBRARY.addBook(new Book("Краткая история времени", "Стивен Хокинг"));
        LIBRARY.addBook(new Book("Финансист", "Теодор Драйзер"));
        LIBRARY.addBook(new Book("Краткая история человечества", "Юваль Ной Харари"));

        //попытка добавить книгу, которая уже есть в библиотеке
        System.out.println();
        System.out.println("ПОПЫТКА ДОБАВЛЕНИЯ КНИГИ, КОТОРАЯ УЖЕ ЕСТЬ В БИБЛИОТЕКЕ:");
        LIBRARY.addBook(new Book("1984", "Джордж Оруэлл"));

        //удаление книги
        System.out.println();
        System.out.println("УДАЛЕНИЕ КНИГИ:");
        LIBRARY.deleteBook("Краткая история человечества");
        LIBRARY.deleteBook("Изучаем Java");
//
        //поиск по названию книги
        System.out.println();
        System.out.println("ПОИСК ПО НАЗВАНИЮ КНИГИ:");
        System.out.println(LIBRARY.nameSearch("Изучаем Java"));
        System.out.println(LIBRARY.nameSearch("Финансист"));

        //поиск по автору
        System.out.println();
        System.out.println("ПОИСК ПО АВТОРУ:");
        LIBRARY.authorSearch("Джордж Оруэлл");
        LIBRARY.authorSearch("Пушкин");

        Visitor visitor1 = new Visitor("Ваня");
        Visitor visitor2 = new Visitor("Гриша");
        Visitor visitor3 = new Visitor("Саша");

        //проверка id посетителя
        System.out.println();
        System.out.println("ПРОВЕРКА ID ПОСЕТИТЕЛЯ:");
        System.out.println(visitor1.getId());
        System.out.println(visitor2.getId());
        System.out.println(visitor3.getId());
        System.out.println(visitor2.getId());
        System.out.println(visitor1.getId());

        //Механизм одалживания книги посетителю
        System.out.println();
        System.out.println("ОДОЛЖИТЬ КНИГУ:");
        LIBRARY.lendBook("1984", visitor1);
        LIBRARY.lendBook("Скотный двор", visitor1);
        LIBRARY.lendBook("1984", visitor2);
        LIBRARY.lendBook("Буратино", visitor2);

        //Вернуть книгу в библиотеку от посетителя
        System.out.println();
        System.out.println("ВЕРНУТЬ КНИГУ:");
        LIBRARY.returnBook("1984", visitor1);
        LIBRARY.returnBook("Финансист", visitor2);

        System.out.println();
        System.out.println("ВЗЯТЬ ДРУГУЮ КНИГУ ПОСЛЕ СДАЧИ ПРОШЛОЙ:");
        LIBRARY.lendBook("Скотный двор", visitor1);

        System.out.println();
        System.out.println("СРЕДНЯЯ ОЦЕНКА КНИГИ:");
        LIBRARY.lendBook("1984", visitor3);
        LIBRARY.returnBook("1984", visitor3);
        LIBRARY.showGradeBook("1984");

    }
}
