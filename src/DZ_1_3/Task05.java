package DZ_1_3;

import java.util.Scanner;
//Даны положительные натуральные числа m и n.
//Найти остаток от деления m на n, не выполняя операцию взятия остатка.

public class Task05 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();
        int  result = m / n;

        result = result * n;
        result = m - result;
        System.out.println(result);
    }
}