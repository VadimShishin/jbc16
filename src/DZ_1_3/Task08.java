package DZ_1_3;

import java.util.Scanner;

//На вход подается: целое число n, целое число p? целые числа a1, a2 , ...an.
//Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго больше p.

public class Task08 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int p = input.nextInt();
        int result = 0;

        for (int i = 0; i < n; i++) {
            int a = input.nextInt();
            if (a > p)
                result += a;
        }
        System.out.println(result);
    }
}