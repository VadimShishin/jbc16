package DZ_1_3;
import java.util.Scanner;
//В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное число n - количество денег для размена.
//Необходимо найти минимальное количество купюр с помощью которых можно разменять это количество денег
// (соблюсти порядок: первым числом вывести количество купюр номиналом 8, вторым - 4 и т д)

public class Task06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();

        int eight = n / 8;
        int four = (n % 8) / 4;
        int two = ((n % 8) % 4) / 2;
        int one = ((n % 8) % 4) % 2;

        System.out.print(eight + " " + four + " " + two + " " + one);
    }
}