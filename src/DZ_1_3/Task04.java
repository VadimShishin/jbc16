package DZ_1_3;

import java.util.Scanner;
//Дано натуральное число n. Вывести его цифры в “столбик

public class Task04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        for(int i = 0; i < s.length(); i++){
            //тернарный оператор
            System.out.println(i < s.length() -1 ? s.substring(i, i + 1) : s.substring(i));

            //обычный способ
//            if (i < s.length() - 1){
//                System.out.println(s.substring(i, i + 1));
//            } else {
//                System.out.println(s.substring(i));
//            }
        }
    }
}
