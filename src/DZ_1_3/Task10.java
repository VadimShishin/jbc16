package DZ_1_3;

import java.util.Scanner;

//Вывести на экран “ёлочку” из символа звездочки (*) заданной высоты N.
//На N + 1 строке у “ёлочки” должен быть отображен ствол из символа |

public class Task10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        String s = "#";
        String space = " ";

        for (int i = 1; i <= a; i++) {
            System.out.println(space.repeat(a - i) + s.repeat(i) + s.repeat(i - 1));
        }
        System.out.println(space.repeat(a-1) + "|");
    }
}