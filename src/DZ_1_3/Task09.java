package DZ_1_3;

import java.util.Scanner;

//На вход последовательно подается возрастающая последовательность из n целых чисел, которая может начинаться с отрицательного числа.
//Посчитать и вывести на экран, какое количество отрицательных чисел было введено в начале последовательности.
//Помимо этого нужно прекратить выполнение цикла при получении первого неотрицательного числа на вход

public class Task09 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = 0;
        int count = 0;

        do {
            int b = input.nextInt();
            a = b;
            count++;
        } while (a < 0);
        System.out.println(--count);
    }
}