package DZ_1_3;

import java.util.Scanner;

//Дана строка s. Вычислить количество символов в ней, не считая пробелов (необходимо использовать цикл).
public class Task07 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String s = input.nextLine();

        System.out.println(s.replaceAll(" ", "").length());
    }
}