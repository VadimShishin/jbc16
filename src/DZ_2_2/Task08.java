package DZ_2_2;

import java.util.Scanner;

/**
 * �� ���� �������� ����� N. ���������� ��������� � ������� �� ����� ����� ��� ����. ������ ������ ����� ����� ��������.
 */

public class Task08 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println(sum(input.nextInt()));
    }

    public static int sum(int n) {
        if (n == 0)
            return 0;
        else {
            return sum(n / 10) + (n % 10);
        }
    }
}
