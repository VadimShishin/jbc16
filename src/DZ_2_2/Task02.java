package DZ_2_2;

import java.util.Scanner;

/**
 * �� ���� �������� ����� N � ���������� ����� � �������� �������.
 * ����� � ����������� ���� ������� �������� ���������� X (����� �������) � Y (����� ������) �����, ������� ������ �������������.
 * ���������� ���������� ������������� � ������� ������� 1 � �������, ����������� ������ (��. ������) � ������� ��� ������� �� �����.
 */
import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        int[] xNumbers = new int[2];
        for (int i = 0; i < xNumbers.length; i++) {
            xNumbers[i] = input.nextInt();
        }

        int[] yNumbers = new int[2];
        for (int i = 0; i < yNumbers.length; i++) {
            yNumbers[i] = input.nextInt();
        }

        int stroka1 = xNumbers[1];
        int stolbec1 = xNumbers[0];

        int stroka2 = yNumbers[1];
        int stolbec2 = yNumbers[0];

        Object[][] matrix = new Object[n][n];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = 0;
            }
        }

        for (int i = stroka1; i <= stroka2; i++) {
            for (int j = stolbec1; j <= stolbec2; j++) {
                if (i == stroka1){
                    matrix[i][j] = 1;
                }
                if (i == stroka2){
                    matrix[i][j] = 1;
                }
                if (j == stolbec1){
                    matrix[i][j] = 1;
                }
                if (j == stolbec2){
                    matrix[i][j] = 1;
                }
            }
        }

        int w = 0;
        Object[][] matrixCopy = new Object[n][n * 2];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrixCopy[i][w] = matrix[i][j];
                matrixCopy[i][w + 1] = " ";
                w = w + 2;
                if (j == matrix[i].length - 1){
                    w = 0;
                }
            }
        }
        Object[][] matrixCopy2 = new Object[n][(n * 2) - 1];

        for (int i = 0; i < matrixCopy.length; i++) {
            System.arraycopy(matrixCopy[i], 0, matrixCopy2[i], 0, matrixCopy[i].length - 1);
        }

        for (int i = 0; i < matrixCopy2.length; i++) {
            for (int j = 0; j < matrixCopy2[i].length; j++) {
                System.out.print(matrixCopy2[i][j]);
            }
            System.out.println();
        }
    }
}