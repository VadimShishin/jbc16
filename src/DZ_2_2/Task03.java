package DZ_2_2;

import java.util.Scanner;

/**
 * �� ���� �������� ����� N � ���������� ����� � �������� �������. ����� ���������� ���������� X � Y ������������ ���� �� ��������� �����.
 * ���������� ��������� ������� ������� NxN ������, �������������� ���� �������� �������� K, � �������, ������� �� ����� ����, �������� X.
 */

public class Task03 {
    public static final int[] str = {2, 1, -1, -2, -2, -1, 1, 2};
    public static final int[] col = {1, 2, 2, 1, -1, -2, -2, -1};

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int N = input.nextInt();
        int[] position = new int[2];

        for (int i = 0; i < position.length; i++) {
            position[i] = input.nextInt();
        }

        String[][] matrix = new String[N][N];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (j == matrix[i].length - 1) {
                    matrix[i][j] = "0";
                } else {
                    matrix[i][j] = "0 ";
                }
            }
        }

        int x = position[1];
        int y = position[0];

        matrix[x][y] = "K ";

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                for (int k = 0; k < str.length; k++) {
                    if (x + str[k] > 0 && x - str[k] < matrix.length && y + col[k] > 0 && y + col[k] < matrix[i].length){
                        matrix[x - str[k]][y + col[k]] = "X ";
                    }
                }
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]);
            }
            System.out.println();
        }
    }
}