package DZ_2_2;

import java.util.Scanner;

/**
 * �� ���� �������� ����� A � ��������� ����� ������, B � ��������� ����� �����, C � ��������� ����� ��������� � K � ��������� ����� �������.
 * ����� ���������� 7 �����, � ������� � ��� �� ������� ������� ������� ���� ������� ����� ���������� � ������ ���� ������.
 * ���� �� ������ � ����� �� ������� ��������� �� ��������� ��������� �����, �� ������� ��������, ����� ������� ������ ���� ��������.
 */

public class Task06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int[] array = new int[4];
        for (int i = 0; i < 4; i++) {
            array[i] = input.nextInt();
        }

        int[][] matrix = new int[7][4];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = input.nextInt();
            }
        }

        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;

        for (int i = 0; i < matrix.length; i++) {
            a += matrix[i][0];
            b += matrix[i][1];
            c += matrix[i][2];
            d += matrix[i][3];
        }

        if (a < array[0] && b < array[1] && c < array[2] && d < array[3]){
            System.out.println("�������");
        } else System.out.println("����� ���� ��������");
    }
}
