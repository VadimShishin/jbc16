package DZ_2_2;

import java.util.Arrays;
import java.util.Scanner;

/**
 * �� ���� ���������� N � ���������� �������� � ��������� ������� � M � ���������� �����. ����� ��� ���������� ��������� ������,
 * ��������� �� ����������� �����.
 * ���������� ��������� � ���������� ������� � ������� �� ����� ����������� ������� ������ ������.
 */

public class Task01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt(); //���������� �����
        int n = input.nextInt(); //���������� ��������

        int[][] numbers = new int[m][n];

        for (int i = 0; i < numbers.length; i++) { // ������ �� �������
            for (int j = 0; j < numbers[i].length; j++) { // ������ �� ��������� �����
                numbers[i][j] = input.nextInt();
            }
        }
//        System.out.println(numbers.length); //���������� ����� (2)
//        System.out.println(numbers[0].length); // ���������� ��������, � ������ �0 (3)

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(arr(numbers, i)[0] + " ");
        }
    }

    /**
     * ����� �������� ���������� ������ � ����� ������� (n)
     * ��������� ���������� ������ �� ����������,
     * ��������� �������� �� �����������
     * � ���������� ��������������� ���������� ������ ��������� �� ��������� (n) ������� ����������� �������
     */
    public static int[] arr(int[][] numbers, int numStr){
        int[] result = new int[numbers[numStr].length];
        for (int i = 0; i < numbers[numStr].length; i++) {
            result[i] = numbers[numStr][i];
        }
        Arrays.sort(result);
        return result;
    }
}
