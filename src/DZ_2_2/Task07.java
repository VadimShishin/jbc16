package DZ_2_2;

import java.util.Scanner;

/**
 * �� ���� �������� ����� N � ���������� ���������� ��������.
 * -	����� � N ������� �������� ����� ������.
 * -	����� ����� � N ������� �������� ������ �����.
 * -	����� ���������� ������� � N �����, 3 ������������ ����� � ������ � ������ �����.
 * ������������ �������� ��� ���������, ��������� ������������ ������� �������������� �� ������� 3 �����.
 * ���������� ������� ���� ����������� � ������� ���� �������: ������, ������� �������.
 * �������������, ��� ������� �������������� ��� ���� ���������� ����� ���������.
 */

public class Task07 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int numberOfRecords = input.nextInt();

        Object[][] matrix = new Object[numberOfRecords][3];

        Object[] names = new Object[numberOfRecords];
        Object[] dogs = new Object[numberOfRecords];
        Object[][] rating = new Object[numberOfRecords][3];

        for (int i = 0; i < names.length; i++) {
            names[i] = input.next();
        }

        for (int i = 0; i < dogs.length; i++) {
            dogs[i] = input.next();
        }

        for (int i = 0; i < rating.length; i++) {
            for (int j = 0; j < 3; j++) {
                rating[i][j] = input.nextDouble();
            }
        }

        Double a = ((Double) rating[0][0] + (Double) rating[0][1]  + (Double) rating[0][2]);
        Double b = ((Double) rating[1][0] + (Double) rating[1][1]  + (Double) rating[1][2]);
        Double c = ((Double) rating[2][0] + (Double) rating[2][1]  + (Double) rating[2][2]);
        Double d = ((Double) rating[3][0] + (Double) rating[3][1]  + (Double) rating[3][2]);

        Object[] finalRating = new Object[numberOfRecords];
        finalRating[0] = a;
        finalRating[1] = b;
        finalRating[2] = c;
        finalRating[3] = d;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (j == 0){
                    matrix[i][j] = names[i];
                }
                if (j == 1){
                    matrix[i][j] = dogs[i];
                }
                if (j == 2){
                    matrix[i][j] = finalRating[i];
                }
            }
        }

        //����������� ������ ��������� �������� ���������� ���������
        for (int i = 0; i < matrix.length; i++) { // ������� ����
            for (int j = 0; j < matrix.length - 1; j++) {
                // ������ ������� � ���� ����������� ������ ���� Object � ���� Double � ����������� � ���������������� ��������� double
                double firstInPair = (Double) matrix[j][2];
                double secondInPair = (Double) matrix[j + 1][2]; // ������ ������� � ����

                if (firstInPair < secondInPair) { //���������� �������� � ����. ���� ������� ��������� ������� ������ ������ �������
                    swap(matrix, j, j + 1);
                }
            }
        }

        double ivan = ((Double) rating[0][0] + (Double) rating[0][1]  + (Double) rating[0][2]);
        double nikolai = ((Double) rating[1][0] + (Double) rating[1][1]  + (Double) rating[1][2]);
        double anna = ((Double) rating[2][0] + (Double) rating[2][1]  + (Double) rating[2][2]);
        double darya = ((Double) rating[3][0] + (Double) rating[3][1]  + (Double) rating[3][2]);


        int[] result = new int[4];
        result[0] = (int) darya;
        result[1] = (int) nikolai;
        result[2] = (int) ivan;
        result[3] = (int) anna;

        // ������� ��������������� ������ �������������
        for (int i = 0; i < matrix.length - 1; i++) {
            String surnameWithInitials = matrix[i][0] + ": ";
            surnameWithInitials += matrix[i][1] + ", ";
            System.out.println(surnameWithInitials + (double) (result[i] * 10 / 3) / 10.0);
        }
    }

    static void swap(Object[][] array, int i, int j) {
        // ���������� ������ �������
        for (int k = 0; k < array[i].length; k++) { // ����������� �� ��������� ���� �����
            // �������� ������� � ������� �������� �������� �������� ������� ������ � �������� i
            Object buff = array[i][k];
            // � ������ � �������� i ���������� �������� �� ������ ������ � �������� j ��� �� �������
            array[i][k] = array[j][k];
            // � ������ � �������� j ���������� �������� ������
            array[j][k] = buff;
        }
    }
}