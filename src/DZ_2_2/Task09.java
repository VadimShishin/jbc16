package DZ_2_2;

import java.util.Scanner;

/**
 * �� ���� �������� ����� N. ���������� ������� ����� ����� ����� �������. ������ ������ ����� ����� ��������
 */

public class Task09 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println(num(input.nextInt()));
    }

    public static String num(int n) {
        if (n < 10)
            return Integer.toString(n); // �������������� � String
        else {
            return num(n / 10) + " " + n % 10;
        }
    }
}
