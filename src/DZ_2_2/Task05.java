package DZ_2_2;

import java.util.Scanner;

/**
 * �� ���� �������� ����� N � ���������� ����� � �������� �������. ����� ���������� ���� �������, ��������� �� ����������� �����.
 * ���������� ������� true, ���� ��� �������� ������������ ������������ �������� ���������, false �����.
 * �������� ���������� ���������� ���������, ���������� �� �������� ������� ���� � ����� ������.
 */

public class Task05 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int N = input.nextInt();

        int[][] matrix = new int[N][N];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = input.nextInt();
            }
        }

        int temp = 1;
        int temp2 = matrix.length - 1;
        boolean result = false;

        for (int i = 0; i < matrix.length; i++) {
            if (i < matrix.length - 1) {
                for (int j = temp2 - 1; j >= 0; j--) {
                    if (j < matrix[i].length - 1) {
                        if (matrix[i][j] != matrix[temp][temp2]) {
                            result = false;
                        } else {
                            result = true;
                        }
                        break;
                    }
                    temp++;
                }
            }
            temp = i + 2;
            temp2--;
        }
        System.out.println(result);
    }
}