package DZ_1_2;

import java.util.Scanner;

//Если время больше полудня, то вывести "Пора". Иначе - “Рано”.

public class Task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        if (i >= 12)
            System.out.println("Пора");
        else
            System.out.println("Рано");
    }
}
