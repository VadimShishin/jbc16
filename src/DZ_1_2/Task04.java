package DZ_1_2;

import java.util.Scanner;

//написать программу, которая по порядковому номеру дня недели выводит сколько осталось дней до субботы.
//А если же сегодня шестой (суббота) или седьмой (воскресенье) день, то программа выводит "Ура, выходные!"

public class Task04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        if (i < 6)
            System.out.println(6 - i);
        else
            System.out.println("Ура, выходные!");
    }
}
