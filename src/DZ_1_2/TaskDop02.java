package DZ_1_2;

import java.util.Scanner;

//Если в посылке есть камни, то будет написано слово "камни!", если запрещенная продукция, то будет фраза "запрещенная продукция".

public class TaskDop02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();

        if (!str.contains("камни")) {
            if (!str.contains("запрещенная продукция")) {
                if (!str.contains("камни") && !str.contains("запрещенная продукция")) {
                    System.out.println("все ок");
                } else {
                    System.out.println("в посылке камни и запрещенная продукция");
                }
            } else {
                System.out.println("в посылке запрещенная продукция");
            }
        } else {
            System.out.println("камни в посылке");
        }
    }
}