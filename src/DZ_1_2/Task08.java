package DZ_1_2;

import java.util.Scanner;

//разделять строку по последнему пробелу

public class Task08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        int index = str.lastIndexOf(" ");
        System.out.println(str.substring(0, index));
        System.out.println(str.substring(index + 1));
    }
}
