package DZ_1_2;

//Напишите программу, которая проверяет, что log(e^n) == n для любого вещественного n.

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double n = scanner.nextDouble();
        double l = Math.log(Math.pow(Math.E, n));
        System.out.println(l == n);
    }
}
