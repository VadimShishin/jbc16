package DZ_1_2;

import java.util.Scanner;

//нужно определить, принадлежит ли точка с указанными координатами первому квадранту.
//на вход нужно принимать два целых числа (координаты точки), выводить true, когда точка попала в квадрант и false иначе.
//точка лежит в первом квадранте тогда, когда её координаты удовлетворяют условию: x > 0 и y > 0

public class Task02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        if (x > 0 && y > 0)
            System.out.println("true");
        else
            System.out.println("false");
    }
}
