package DZ_1_2;

import java.util.Scanner;

//Напишите программу, которая проверяет, что при любом x на входе тригонометрическое тождество будет выполняться (то есть будет выводить true при любом x).
//(sin^2(x)+ cos^2(x) - 1 == 0)

public class Task09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();

        int result = (int) (Math.pow(Math.sin(x),2) + Math.pow(Math.cos(x),2) - 1);
        int zero = 0;
        System.out.println(result == 0);
    }
}
