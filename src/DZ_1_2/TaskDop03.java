package DZ_1_2;

import java.util.Scanner;

public class TaskDop03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String model = input.nextLine();
        int price = input.nextInt();

        if (model.contains("samsung") || model.contains("iphone")){
            if (price >= 50000 && price <= 120000){
                System.out.println("Можно купить");
            } else {
                System.out.println("Не подходит");
            }
        } else {
            System.out.println("Не подходит");
        }
    }
}