package DZ_1_2;

import java.util.Scanner;

//имеет ли предложенное квадратное уравнение решение или нет.
//На вход подаются три числа — коэффициенты квадратного уравнения a, b, c. Нужно вывести "Решение есть", если оно есть и "Решения нет", если нет.

public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();
        double d;
        d = Math.pow(b, 2) - 4 * a * c;

        if (d > 0 || d == 0)
            System.out.println("Решение есть");
        else
            System.out.println("Решения нет");
    }
}