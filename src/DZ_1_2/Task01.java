package DZ_1_2;

import java.util.Scanner;

//Если последовательность оценок строго монотонно убывает, то вывести "Петя, пора трудиться"
//В остальных случаях вывести "Петя молодец!"

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int ball1 = scanner.nextInt();
        int ball2 = scanner.nextInt();
        int ball3 = scanner.nextInt();
        if (ball1 > ball2 && ball2 > ball3)
            System.out.println("Петя, пора трудиться");
        else
            System.out.println("Петя молодец!");
    }
}
