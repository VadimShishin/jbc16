package DZ_1_2;
//На вход подается три целых положительных числа – длины сторон треугольника. Нужно вывести true, если можно составить
// треугольник из этих сторон и false если иначе.

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println(a + b > c && a + c > b && b + c > a);
    }
}
