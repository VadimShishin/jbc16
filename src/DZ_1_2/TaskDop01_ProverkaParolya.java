package DZ_1_2;

import java.util.Scanner;

//пароль должен отвечать следующим требованиям: пароль должен состоять из хотя бы 8 символов;
//в пароле должны быть: заглавные буквы, строчные символы, числа, специальные знаки(_*-).
//Если пароль прошел проверку, то программа должна вывести в консоль строку пароль надежный, иначе строку: пароль не прошел проверку

public class TaskDop01_ProverkaParolya {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String str2 = str.toLowerCase();
        String str3 = str.toUpperCase();

        if (str.length() >= 8) {
            if (str.contains("0") || str.contains("1") || str.contains("2") || str.contains("3") || str.contains("4") || str.contains("5") || str.contains("6") || str.contains("7") || str.contains("8") || str.contains("9")) {
                if (str.contains("*") || str.contains("-") || str.contains("_")) {
                    if (str != str2) {
                        if (str != str3) {
                            System.out.println("пароль надежный");
                        } else {
                            System.out.println("пароль не прошел проверку");
                        }
                    } else {
                        System.out.println("пароль не прошел проверку");
                    }
                } else {
                    System.out.println("пароль не прошел проверку");
                }
            } else {
                System.out.println("пароль не прошел проверку");
            }
        } else {
            System.out.println("пароль не прошел проверку");
        }
    }
}
