package DZ_1_2;

import java.util.Scanner;

//На вход подается строка. Нужно вывести две строки, полученные из входной разделением по первому пробелу
//воспользоваться методами indexOf() и substring()

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        int index = str.indexOf(" ");
        System.out.println(str.substring(0, index));
        System.out.println(str.substring(index + 1));
    }
}