package DZ_3_3.Task04;

public class Participant {
    private String name;
    Dog dog;

    public Participant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }
}
