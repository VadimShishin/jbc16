package DZ_3_3.Task04;

import java.util.ArrayList;
import java.util.Scanner;

public class Dog {
    Scanner input = new Scanner(System.in);
    private String nameDog;
    private ArrayList<Integer> ratingDog;

    public Dog(String nameDog) {
        this.nameDog = nameDog;
    }

    public String getNameDog() {
        return nameDog;
    }

    public ArrayList<Integer> getRatingDog() {
        return ratingDog;
    }

    public void setRatingDog(ArrayList<Integer> ratingDog) {
        this.ratingDog = ratingDog;
    }

    public double averageRating(Dog dog){
        int sum = 0;
        for (int i = 0; i < dog.getRatingDog().size(); i++) {
            sum += dog.getRatingDog().get(i);
        }

        return (double) (sum * 10 / dog.getRatingDog().size()) / 10.0;
    }
}
