package DZ_3_3.Task04;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int N = input.nextInt();
        ArrayList<Participant> participants = new ArrayList<>();
        ArrayList<Dog> dogs = new ArrayList<>();
        ArrayList<ArrayList<Integer>> rating = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            participants.add(new Participant(input.next()));
        }

        for (int i = 0; i < N; i++) {
            dogs.add(new Dog(input.next()));
        }

        for (int i = 0; i < N; i++) {
            rating.add(new ArrayList<>());
            for (int j = 0; j < 3; j++) {
                rating.get(i).add(input.nextInt());
            }
        }

        for (int i = 0; i < N; i++) {
            dogs.get(i).setRatingDog(rating.get(i));
        }

        for (int i = 3; i >= 0; i--) {
            for (int j = 0; j < N; j++) {
                if (dogs.get(i).averageRating(dogs.get(i)) > dogs.get(j).averageRating(dogs.get(j)) && dogs.get(i).averageRating(dogs.get(i)) != dogs.get(j).averageRating(dogs.get(j))){
                    System.out.println(participants.get(i).getName() + ": " + dogs.get(i).getNameDog() + ", " + dogs.get(i).averageRating(dogs.get(i)));
                    break;
                }
            }
        }
    }
}