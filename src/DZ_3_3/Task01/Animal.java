package DZ_3_3.Task01;

public abstract class Animal {
    abstract void eat();
    abstract void sleep();
}
