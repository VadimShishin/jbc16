package DZ_3_3.Task01;

public class Dolphin extends Mammal implements Swimming{
    @Override
    void eat() {
        System.out.println("Ест");
    }

    @Override
    void sleep() {
        System.out.println("Спит");
    }

    @Override
    void liveBirth() {
        System.out.println("Рожает");
    }

    @Override
    public void swim() {
        System.out.println("Плавает быстро");
    }
}
