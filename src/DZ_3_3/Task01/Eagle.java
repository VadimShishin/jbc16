package DZ_3_3.Task01;

public class Eagle extends Bird implements Flying {

    @Override
    void eat() {
        System.out.println("Ест");
    }

    @Override
    void sleep() {
        System.out.println("Спит");
    }

    @Override
    void laysEggs() {
        System.out.println("Откладывает яйца");
    }

    @Override
    public void fly() {
        System.out.println("Летит быстро");
    }
}
