package DZ_3_3.Task01;

public class Bat extends Mammal implements Flying{
    @Override
    void eat() {
        System.out.println("Ест");
    }

    @Override
    void sleep() {
        System.out.println("Спит");
    }

    @Override
    void liveBirth() {
        System.out.println("Рожает");
    }

    @Override
    public void fly() {
        System.out.println("Летит медленно");
    }
}
