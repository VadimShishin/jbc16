package DZ_3_3.Task01;

public class GoldFish extends Fish implements Swimming {
    @Override
    void eat() {
        System.out.println("Ест");
    }

    @Override
    void sleep() {
        System.out.println("Спит");
    }

    @Override
    void caviar() {
        System.out.println("Мечет игру");
    }

    @Override
    public void swim() {
        System.out.println("Плавает медленно");
    }
}
