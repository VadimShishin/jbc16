package DZ_3_3;

import java.util.ArrayList;
import java.util.Scanner;

//На вход передается N — количество столбцов в двумерном массиве и M — количество строк. Необходимо вывести матрицу на экран,
//каждый элемент которого состоит из суммы индекса столбца и строки этого же элемента. Решить необходимо используя ArrayList.
public class Task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<ArrayList<Integer>> list = new ArrayList<>();
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        for (int i = 0; i < m; i++) {
            list.add(new ArrayList<>());
            for (int j = 0; j < n; j++) {
                list.get(i).add(i + j);
            }
        }

        for (ArrayList<Integer> integers : list) {
            for (Integer integer : integers) {
                System.out.print(integer + " ");
            }
            System.out.println();
        }
    }
}