package DZ_3_3.Task02;

public class Main {
    public static void main(String[] args) {
        BestCarpenterEver bestCarpenterEver = new BestCarpenterEver();
        Stools stools = new Stools();
        Table table = new Table();
        System.out.println(bestCarpenterEver.fixFurniture(stools));
        System.out.println(bestCarpenterEver.fixFurniture(table));
    }
}
