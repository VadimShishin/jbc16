package DZ_1_1;

import java.util.Scanner;

//На вход подается бюджет мероприятия – n тугриков. Бюджет на одного гостя – k тугриков. Вычислите и выведите, сколько гостей можно пригласить на мероприятие

public class Task09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int budget = scanner.nextInt();
        int budgetOneGuest = scanner.nextInt();
        System.out.println(budget / budgetOneGuest);
    }
}
