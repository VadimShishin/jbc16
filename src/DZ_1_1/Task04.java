package DZ_1_1;

import java.util.Scanner;

//На вход подается количество секунд, прошедших с начала текущего дня – count. Выведите в консоль текущее время в формате: часы и минуты

public class Task04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        int hour = count / 3600;
        int value = count / 60;
        int minute = value % 60;
        System.out.println(hour + " " + minute);
    }
}
