package DZ_1_1;

import java.util.Scanner;

//На вход подается баланс счета в банке – n. Рассчитайте дневной бюджет на 30 дней

public class Task08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double balance = scanner.nextInt();
        double budget = balance / 30;
        System.out.println(budget);
    }
}
