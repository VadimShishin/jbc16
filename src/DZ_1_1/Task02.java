package DZ_1_1;

import java.util.Scanner;

//На вход подается два целых числа a и b. Вычислите и выведите среднее квадратическое a и b.

public class Task02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        double s = Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2);
        System.out.println(s);
    }
}
