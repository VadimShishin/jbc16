package DZ_1_1;

import java.util.Scanner;

//Прочитайте из консоли имя пользователя и выведите в консоль строку: Привет, <имя пользователя>!

public class Task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        System.out.println("Привет, " + s + "!");
    }
}
