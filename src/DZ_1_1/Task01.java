package DZ_1_1;
import java.util.Scanner;

//Вычислите и выведите на экран объем шара, получив его радиус r с консоли.

public class Task01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double r = scanner.nextDouble();
        double volume = (4.0 / 3.0) * Math.PI * Math.pow(r, 3);
        System.out.println(volume);
    }
}
