package DZ_1_1;

import java.util.Scanner;

//На вход подается двузначное число n. Выведите число, полученное перестановкой цифр в исходном числе n. Если после перестановки получается ведущий 0, его также надо вывести

public class Task07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int firstNumber = a / 10;
        int secondNumber = a % 10;
        System.out.print(secondNumber);
        System.out.print(firstNumber);
    }
}
