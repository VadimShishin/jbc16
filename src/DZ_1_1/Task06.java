package DZ_1_1;

import java.util.Scanner;

//На вход подается количество километров count. Переведите километры в мили (1 миля = 1,60934 км) и выведите количество мил

public class Task06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final double km = 1.60934;
        int m = scanner.nextInt();
        System.out.println(m / km);
    }
}
