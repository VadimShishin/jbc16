package DZ_1_1;

import java.util.Scanner;

//Переведите дюймы в сантиметры (1 дюйм = 2,54 сантиметров). На вход подается количество дюймов, выведите количество сантиметров

public class Task05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final double cm = 2.54;
        int d = scanner.nextInt();
        System.out.println(d * cm);
    }
}
