package DZ_3_2;

import static DZ_3_2.Library.LIBRARY;

public class Main {
    public static void main(String[] args) {
        //добавление книг
        LIBRARY.addBook(new Book("1984", "Джордж Оруэлл"));
        LIBRARY.addBook(new Book("Краткая история времени", "Стивен Хокинг"));
        LIBRARY.addBook(new Book("Финансист", "Теодор Драйзер"));
        LIBRARY.addBook(new Book("Краткая история человечества", "Юваль Ной Харари"));

//        //попытка добавить книгу, которая уже есть в библиотеке
//        System.out.println();
//        LIBRARY.addBook(new Book("1984", "Джордж Оруэлл"));
//
//        //показать все книги
//        System.out.println();
//        LIBRARY.showBooks();
//
//        //удаление книги
//        System.out.println();
//        LIBRARY.deleteBook("1984");
//        LIBRARY.deleteBook("Изучаем Java");
//
//        //поиск по названию книги
//        System.out.println();
//        System.out.println(LIBRARY.nameSearch("Изучаем Java"));
//        System.out.println(LIBRARY.nameSearch("1984"));
//
//        //поиск по автору
//        System.out.println();
//        System.out.println(LIBRARY.authorSearch("Джордж Оруэлл"));
//        System.out.println(LIBRARY.authorSearch("Пушкин"));

        Visitor visitor1 = new Visitor("Иванов Иван Иванович");
//        Visitor visitor2 = new Visitor("Петров Петр Петрович");

    }
}
