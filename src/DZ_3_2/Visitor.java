package DZ_3_2;

public class Visitor {
    private String name;
    private String identifier;

    public Visitor(String name) {
        this.name = name;
    }

    public Visitor(String name, String identifier) {
        this.name = name;
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

}
