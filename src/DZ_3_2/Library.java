package DZ_3_2;

import java.util.ArrayList;

public class Library {
    public static final Library LIBRARY = new Library();
    private Library() {}
    public ArrayList<Book> books = new ArrayList<>();

    public void addBook(Book book) {
        boolean contains = false;
        for (Book b : books) {
            if (b.getName().equals(book.getName())) {
                contains = true;
                break;
            }
        }

        if (contains) {
            System.out.println("Книга \"" + book.getName() + "\" не добавлена, так как она уже есть в библиотеке.");
        } else {
            books.add(book);
            System.out.println("Книга \"" + book.getName() + "\" добавлена в библиотеку.");
        }
    }

    public void deleteBook(String nameBook) {
        boolean contains = false;
        for (Book b : books) {
            if (b.getName().equalsIgnoreCase(nameBook)) {
                contains = true;
                books.remove(b);
                break;
            }
        }

        if (contains)
            System.out.println("Книга \"" + nameBook + "\" удалена из библиотеки.");
        else
            System.out.println("Невозможно удалить книгу \"" + nameBook + "\", так как она не найдена в библиотеке.");
    }

    public void showBooks(){
        System.out.println("В библиотеке содержатся следующие книги:");
        for (Book b : books) {
            System.out.println(b.getName() + ", " + b.getAuthor());
        }
    }

    public String nameSearch (String nameBook) {
        String book = null;
        for (Book b : books) {
            if (b.getName().equalsIgnoreCase(nameBook)) {
                book = b.getName() + ", " + b.getAuthor();
                break;
            }
        }
        if (book == null)
            return "Книга \"" + nameBook + "\" не найдена.";
        else
            return ("Найдена книга: " + book);
    }

    public String authorSearch (String authorBook) {
        String author = null;
        for (Book b : books) {
            if (b.getAuthor().equalsIgnoreCase(authorBook)) {
                author = b.getName() + ", " + b.getAuthor();
                break;
            }
        }
        if (author == null)
            return "Книга \"" + authorBook + "\" не найдена.";
        else
            return ("Найдена книга: " + author);
    }

    public void lendBook(String nameBook, Visitor visitor) {
        System.out.println("Одолжена книга: " + nameBook);
    }
}